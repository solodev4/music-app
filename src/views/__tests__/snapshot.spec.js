import { shallowMount, RouterLinkStub } from '@vue/test-utils'
import SongItem from '@/components/SongItem.vue'

describe('Snapshots from SongItem', () => {
    it('Renders Correctly', () => {
        const song = {
            id: 'abc123',
            modified_name: 'Tester test',
            comment_count: 3,
            display_name: 'Some diff test',
        }

        const wrapper = shallowMount(SongItem, {
            propsData: { song },
            global: {
                components: {
                    'router-link': RouterLinkStub
                }
            }
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})