// We need to mount the component in the virtual dom, shallowMount does not mount the child compoments
import About from '@/views/About.vue'
import { shallowMount } from '@vue/test-utils'

describe('Test: About component tests', () => {
  it('renders inner text', () => {
    const wrapper = shallowMount(About)
    // const wrapper = mount(About, {shallow: true}) // same as above
    expect(wrapper.text()).toContain('This is an about page')
  })
})
