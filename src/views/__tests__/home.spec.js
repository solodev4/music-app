// We need to mount the component in the virtual dom, shallowMount does not mount the child compoments
import Home from '@/views/Home.vue'
import SongItem from '@/components/SongItem.vue'
import { shallowMount } from '@vue/test-utils'

describe('Tests: Home', () => {
  it('renders list of songs', () => {
    const songs = [{}, {}, {}]
    const component = shallowMount(Home, {
      data() {
        return {
          songs,
        }
      },
      global: {
        // Mocking global functions
        mocks: {
          $t: (message) => message,
        },
      },
    })

    const items = component.findAllComponents(SongItem)
    expect(items).toHaveLength(songs.length)
  })

  it('renders list of songs in correct order', () => {
    const songs = [{}, {}, {}]
    const component = shallowMount(Home, {
      data() {
        return {
          songs,
        }
      },
      global: {
        // Mocking global functions
        mocks: {
          $t: (message) => message,
        },
      },
    })

    const items = component.findAllComponents(SongItem)
    items.forEach((wrapper, index) => {
      expect(wrapper.props().song).toStrictEqual(songs[index])
    })
  })
})
