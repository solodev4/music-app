export default {
  beforeMount(el, binding) {
    // Runs before the element is added to the document
    let iconClass = `fa fa-${binding.value.icon} text-xl  text-green-400`

    if (binding.value.right) {
      iconClass += ' float-right'
    }

    el.innerHTML += `<i class="${iconClass}"></i>`
  },
}
