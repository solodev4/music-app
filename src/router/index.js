import { createRouter, createWebHistory } from 'vue-router'
import home from '@/views/Home.vue'
import about from '@/views/About.vue'
import manage from '@/views/Manage.vue'
import song from '@/views/Song.vue'
import { useUserStore } from '@/stores/user'

const routes = [
  {
    name: 'home',
    path: '/',
    component: home,
  },
  {
    name: 'about',
    path: '/about',
    component: about,
  },
  {
    name: 'manage',
    // alias: '/manage',
    path: '/manage-music',
    component: manage,
    beforeEnter: (to, from, next) => {
      next() // TODO: test out removing this call to the 'next' function
    },
    meta: {
      requiresAuth: true,
    },
  },
  {
    // Redirecting from old path to the new path
    // Better for SEO
    path: '/manage',
    redirect: { name: 'manage' },
  },
  {
    path: '/:catchAll(.*)*',
    redirect: { name: 'home' },
  },
  {
    name: 'song',
    path: '/song/:id',
    component: song,
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  linkExactActiveClass: 'text-yellow-500',
})

router.beforeEach((to, from, next) => {
  // console.log(to.meta)
  if (!to.meta.requiresAuth) {
    // record doesnt require authentication
    next()
    return
  }

  const store = useUserStore()
  if (store.userLoggedIn) {
    next()
  } else {
    next({ name: 'home' })
  }
}) // happens between each route

export default router
