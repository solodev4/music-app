import { shallowMount, RouterLinkStub } from '@vue/test-utils'
import SongItem from '@/components/SongItem.vue'

describe('router', () => {
    it('Render correct router link', () => {
        const song = { id: 'abc123'};
        const wrapper = shallowMount(SongItem, {
            propsData: { song },
            global: {
                components: {
                    // Fake component
                    'router-link': RouterLinkStub
                }
            }
        });

        expect(wrapper.findComponent(RouterLinkStub).props().to).toEqual({ name: 'song', params: { id: song.id } })
    })
})

