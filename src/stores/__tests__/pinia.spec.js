import { setActivePinia, createPinia } from "pinia";
import { useUserStore } from "@/stores/user";

// You do not need to mount a component (i.e. virtual DOM for testing the store)
// vi is in vitest package
vi.mock('@/includes/firebase', () => ({
    auth: {
        signInWithEmailAndPassword: () => Promise.resolve()
    }
}))

describe('stores', () => {
    beforeEach(() => {
        setActivePinia(createPinia())
    })

    it('authenticate user', async () => {
        const store = useUserStore();
        expect(store.userLoggedIn).toBe(false);
        await store.authenticate({})
        expect(store.userLoggedIn).toBe(true);
    } )
});