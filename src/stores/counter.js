import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }

  return { count, doubleCount, increment }
})

export const legacyCounterStore = defineStore({
  id: 'legacyCounter',
  state: () => ({
    lCounter: 0,
  }),
  getters: {
    lDoubleCount: (state) => state.lCounter * 2,
  },
  actions: {
    increment() {
      this.lCounter++
    },
  },
})
