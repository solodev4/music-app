import { defineStore } from 'pinia'
import { Howl } from 'howler'
import Helpers from '@/includes/helper'

export const usePlayerStore = defineStore('player', {
  actions: {
    async newSong(song) {
      if (this.sound instanceof Howl) {
        this.sound.unload()
      }
      this.currentSong = song
      this.sound = new Howl({
        src: [song.url],
        html5: true,
      })

      this.sound.play()
      this.sound.on('play', () => {
        requestAnimationFrame(this.progress)
      })
    },
    async toggleAudio() {
      if (!this.sound.playing) {
        // Checks if the function exists on the object
        return
      }

      if (this.sound.playing()) {
        // Invokes the function and check if the audio is playing
        this.sound.pause()
      } else {
        this.sound.play()
      }
    },
    updateSeek(event) {
      if (this.sound.playing) {
        // Provide visual cue that the timeline was clicked
        // x: the left position of the element with the listener
        const { x, width } = event.currentTarget.getBoundingClientRect()
        const clickX = event.clientX - x
        const percentage = clickX / width
        const seconds = percentage * this.sound.duration()

        this.sound.seek(seconds)
        this.sound.once('seek', this.progress)
      }
    },
    progress() {
      this.seek = Helpers.formatSeconds(this.sound.seek())
      this.duration = Helpers.formatSeconds(this.sound.duration() - this.sound.seek())
      this.playerProgress = `${(this.sound.seek() / this.sound.duration()) * 100}%`
      if (this.sound.playing()) {
        requestAnimationFrame(this.progress)
      }
    },
  },
  state: () => ({
    currentSong: {},
    sound: {},
    seek: '00:00',
    duration: '00:00',
    playerProgress: '0%',
  }),
  getters: {
    playing: (state) => {
      if (state.sound.playing) {
        return state.sound.playing()
      }

      return false
    },
  },
})
