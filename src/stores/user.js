import { defineStore } from 'pinia'
import { auth, usersCollection } from '@/includes/firebase'

export const useUserStore = defineStore('user', {
  state: () => ({
    userLoggedIn: false,
  }),
  actions: {
    async signOut() {
      await auth.signOut()
      this.userLoggedIn = false
    },
    async authenticate(values) {
      await auth.signInWithEmailAndPassword(values.email, values.password)
      this.userLoggedIn = true
    },
    async register(values) {
      const userCredentials = await auth.createUserWithEmailAndPassword(
        values.email,
        values.password
      )

      // At this point we can log other values in the DB.
      await usersCollection.doc(userCredentials.user.uid).set({
        name: values.name,
        email: values.email,
        age: values.age,
        country: values.country,
      })

      await userCredentials.user.updateProfile({ displayName: values.name })
      this.userLoggedIn = true
    },
  },
})
