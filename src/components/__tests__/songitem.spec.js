import SongItem from '@/components/SongItem.vue'
import { shallowMount, RouterLinkStub } from '@vue/test-utils'

describe('Test: SongItem component', () => {
    it('Render author name', () => {
        const song = {display_name: 'Test Author'}
        const wrapper = shallowMount(SongItem, {
            propsData: {
                song
            },
            global: {
                components: {
                    // Fake component
                    'router-link': RouterLinkStub
                }
            }
        })
        const authorObj = wrapper.find('.text-gray-500');
        expect(authorObj.text()).toBe(song.display_name)
    })

    it('Renders song.id as a dynamic attribute', () => {
        const song = {id: '123-test-abc'}
        const wrapper = shallowMount(SongItem, {
            propsData: {
                song
            },
            global: {
                components: {
                    // Fake component
                    'router-link': RouterLinkStub
                }
            }
        })
        // Since the wrapper is the component's root element, we can easily grab the attributes from taht
        // You cannot grab child element attributes from the wrapper
        expect(wrapper.attributes().id).toBe(`song-id-${song.id}`)
        // Exmaple when testing dynamic classes
        //expect(wrapper.classes()).toContain(`song-id-${song.id}`)
    })
    
})