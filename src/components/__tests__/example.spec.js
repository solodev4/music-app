// Sanity test to verify whether test is failing or the code is failing the test
// import { test } from "vitest";
// test Imported globally in the vite config file
describe('Sanity test', () => {
  it('Expect true to be true', () => {
    expect(true).toBe(true)
  })

  it('Expect false to be false', () => {
    expect(false).toBe(false)
  })

  it('Expect sum to be correct', () => {
    expect(1 + 2).to.equal(3)
  })
})
