import firebase from 'firebase/app'
import 'firebase/auth'
// import 'firebase/database' // used for the realtime (older version) database
import 'firebase/firestore'
import 'firebase/storage'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCkrIBIVbl_DvOkkLGAV-KpyjRPqBkD5ek', // TODO: Make secret
  authDomain: 'music-udemy-dg.firebaseapp.com',
  projectId: 'music-udemy-dg',
  storageBucket: 'music-udemy-dg.appspot.com', // TODO: Make secret
  // messagingSenderId: "955169436931", required for push notifications from firebase
  appId: '1:955169436931:web:baa6a1632f721aec7e2536', // TODO: make secret
}

firebase.initializeApp(firebaseConfig)

const auth = firebase.auth()
const db = firebase.firestore()
const storage = firebase.storage()
// Create variables to access existing firebase collections
const usersCollection = db.collection('users')
const songsCollection = db.collection('songs')
const commentsCollection = db.collection('comments')

export { auth, db, usersCollection, songsCollection, commentsCollection, storage }
