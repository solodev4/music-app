import {
  Form as VeeForm,
  Field as VeeField,
  defineRule,
  ErrorMessage,
  configure,
} from 'vee-validate'
import {
  required,
  min,
  max,
  alpha_spaces as alphaSpaces,
  email,
  numeric,
  min_value as minValue,
  max_value as maxValue,
  confirmed,
  not_one_of as excluded,
  regex,
} from '@vee-validate/rules'

export default {
  // App - reference to the view app
  // Options - object that needs additional parameters
  // install(app, options) {
  install(app) {
    app.component('VeeForm', VeeForm)
    app.component('VeeField', VeeField)
    app.component('ErrorMessage', ErrorMessage)

    defineRule('required', required)
    defineRule('tos', required)
    defineRule('min', min)
    defineRule('max', max)
    defineRule('alpha_spaces', alphaSpaces)
    defineRule('email', email)
    defineRule('numeric', numeric)
    defineRule('min_value', minValue)
    defineRule('max_value', maxValue)
    defineRule('passwordsMismatched', confirmed)
    defineRule('excluded', excluded)
    defineRule('regex', regex)
    defineRule('countryExcluded', excluded)

    configure({
      generateMessage: (ctx) => {
        const messages = {
          required: `${ctx.field} is required.`,
          min: `${ctx.field} is too short.`,
          max: `${ctx.field} is too long.`,
          alpha_spaces: `${ctx.field} may only have alphabetical characters and spaces.`,
          email: `${ctx.field} must be a valid email.`,
          numeric: `${ctx.field} may only be a numeric value.`,
          min_value: `${ctx.field} value is too low.`,
          max_value: `${ctx.field} value is too high.`,
          excluded: `You are not allowed to use this value for the field ${ctx.field}.`,
          countryExcluded: `Due to restrictions, we cannot accept users from this country.`,
          passwordsMismatched: `Your passwords dont match.`,
          tos: `You must accept the terms of service.`,
          regex: `The ${ctx.field} format is invalid.`,
        }

        const message = messages[ctx.rule.name]
          ? messages[ctx.rule.name]
          : `The field ${ctx.field} is invalid`
        return message
      },
    })
  },
}
