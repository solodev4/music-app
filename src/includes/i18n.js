import { createI18n } from 'vue-i18n'
import en from '@/includes/locales/en.json'
import fr from '@/includes/locales/fr.json'
import enNumeric from '@/includes/numeric/en.json'
import frNumeric from '@/includes/numeric/fr.json'

export default createI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en,
    fr,
  },
  numberFormats: {
    en: enNumeric,
    fr: frNumeric,
  },
})
